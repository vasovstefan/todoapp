import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:todoapp/services/auth.dart';


import 'auth_test.mocks.dart';


@GenerateMocks([FirebaseAuth, UserCredential])
void main() {
  late final Auth auth;
  late final MockUserCredential mockCredentials;
  late final MockFirebaseAuth mockFirebase;

  setUp(() {
    mockFirebase = MockFirebaseAuth();
    mockCredentials = MockUserCredential();
    auth = Auth(auth: mockFirebase);
  });
  group('auth', () {
    test("create account exception", () async {
      when(
        mockFirebase.createUserWithEmailAndPassword(
          email: "stefan55@gmail.com",
          password: "123456",
        ),
      ).thenAnswer((_) => throw FirebaseAuthException(code: 'Error'));
      expect(
        await auth.createAccount(
          email: "stefan55@gmail.com",
          password: "123456",
        ),
        false,
      );
    });
    test("create account success", () async {
      when(
        mockFirebase.createUserWithEmailAndPassword(
          email: "stefan55@gmail.com",
          password: "123456",
        ),
      ).thenAnswer(
        (_) async => mockCredentials,
      );
      expect(
        await auth.createAccount(
          email: "stefan55@gmail.com",
          password: "123456",
        ),
        true,
      );
    });
    test("signin account", () async {
      when(
        mockFirebase.signInWithEmailAndPassword(
          email: "stefan55@gmail.com",
          password: "123456",
        ),
      ).thenAnswer((_) async => mockCredentials);
      expect(
        await auth.signIn(
          email: "stefan55@gmail.com",
          password: "123456",
        ),
        true,
      );
    });

    test("signin account exception", () async {
      when(
        mockFirebase.signInWithEmailAndPassword(
          email: "stefan55@gmail.com",
          password: "123456",
        ),
      ).thenAnswer((_) => throw FirebaseAuthException(code: 'Error'));
      expect(
        await auth.signIn(
          email: "stefan55@gmail.com",
          password: "123456",
        ),
        false,
      );
    });
    test("logout", () async {
      when(
        mockFirebase.signOut(),
      ).thenAnswer((_) async => true);
      expect(
        await auth.signOut(),
        true,
      );
    });
    test("logout exception", () async {
      when(
        mockFirebase.signOut(),
      ).thenAnswer((_) => throw FirebaseAuthException(code: 'Error'));
      expect(
        await auth.signOut(),
        false,
      );
    });
  });
}
