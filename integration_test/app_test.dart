import 'package:integration_test/integration_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:todoapp/main.dart';

final usernameField = find.byKey(const ValueKey('username'));
final passwordField = find.byKey(const ValueKey('password'));
final signInButton= find.byKey(const ValueKey('signIn'));
final createAccountButton= find.byKey(const ValueKey('createAccount'));
final addFieldButton= find.byKey(const ValueKey('addField'));
final addButton = find.byKey(const ValueKey('addButton'));
final removeTask = find.byKey(const ValueKey('checkBox'));
final signOutButton = find.byKey(const ValueKey('signOut'));

const testUsername = 'stefanvasov2@gmail.com';
const testPassword = '1234567';


Future<void> logout(WidgetTester tester) async {
  await Future<void>.delayed(const Duration(seconds: 2));
  await tester.tap(signOutButton);

  await Future<void>.delayed(const Duration(seconds: 2));
  await tester.pumpAndSettle();
}

void main() {
  final binding = IntegrationTestWidgetsFlutterBinding.ensureInitialized();
  binding.framePolicy = LiveTestWidgetsFlutterBindingFramePolicy.fullyLive;
  WidgetsFlutterBinding.ensureInitialized();
  group('todoapp', () {
    testWidgets('Create Account', (tester) async {
     
      await Firebase.initializeApp();
      await tester.pumpWidget(MyApp());
      await tester.pumpAndSettle();
      
      await tester.enterText(
        find.byKey(const ValueKey('username')),
        testUsername,
      );
      await tester.enterText(
        find.byKey(const ValueKey('password')),
        testPassword);
      await tester.tap(createAccountButton);
      await Future<void>.delayed(const Duration(seconds: 2));
      await tester.pumpAndSettle();

      expect(find.text('Add Todo Here:'), findsOneWidget);
    });
testWidgets('Add task', (WidgetTester tester) async {
    await tester.pumpWidget(MyApp());
    await tester.pumpAndSettle();

    await tester.enterText(addFieldButton, 'Test');
      
    await tester.tap(addButton);
    await Future<void>.delayed(const Duration(seconds: 1));
    await tester.pumpAndSettle();
      
    expect(find.text('Test'), findsOneWidget);

});
testWidgets('Delete task', (WidgetTester tester) async {
    await tester.pumpWidget(MyApp());
    await tester.pumpAndSettle();
    
    await Future<void>.delayed(const Duration(seconds: 2));
    await tester.pumpAndSettle();
    await tester.tap(removeTask);
    await Future<void>.delayed(const Duration(seconds: 2));
    await tester.pumpAndSettle(); 

    await Future<void>.delayed(const Duration(seconds: 2));
    await tester.pumpAndSettle(); 

    expect(find.text('Amateur Coder Todo'), findsOneWidget);

    await logout(tester);

});
   
testWidgets('SignIn', (WidgetTester tester) async {
    
    await tester.pumpWidget(MyApp());
    await tester.pumpAndSettle();
    
    await tester.pumpAndSettle();
    await tester.enterText(find.byKey(const ValueKey('username')), testUsername);
    await tester.enterText(find.byKey(const ValueKey('password')), testPassword);
    await tester.tap(find.byKey(const ValueKey('signIn')));

    await Future<void>.delayed(const Duration(seconds: 2));
    await tester.pumpAndSettle(); 

    expect(find.text('Amateur Coder Todo'), findsOneWidget);

    await logout(tester);


});

  });
}
