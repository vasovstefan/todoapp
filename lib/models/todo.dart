import 'package:cloud_firestore/cloud_firestore.dart';

class TodoModel {
  String? todoId;
  String? content;
  bool? done;

  TodoModel({required this.todoId, required this.content, required this.done});

  factory TodoModel.fromSnapshot(DocumentSnapshot snapshot) {
    return TodoModel(
      todoId: snapshot.id,
      content: snapshot['content'] as String,
      done: snapshot['done'] as bool,
    );
  }
}
